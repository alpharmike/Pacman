#include <windows.h>
#include <iostream>
#include <gl\glut.h>
#include "Ellipses.h"

#define M_PI 3.14159265358979323846

using namespace std;

void Ellipses::setPixel(int x, int y)
{
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_POINTS);
	glVertex2f(x, y);
	glEnd();
}

void Ellipses::MidPoint(int cx, int cy, int rx, int ry)
{
	int x = 0;
	int y = ry;

	float P = ry*ry - rx*rx*ry + rx*rx / 4;
	while (ry*ry*x < rx*rx*y)
	{
		Ellipses::setPixel(cx + x, cy + y);
		Ellipses::setPixel(cx - x, cy + y);
		Ellipses::setPixel(cx + x, cy - y);
		Ellipses::setPixel(cx - x, cy - y);

		x++;
		if (P < 0)
		{
			P += 2 * ry*ry *x + ry*ry;
		}
		else
		{
			y--;
			P += 2 * ry*ry *x + ry*ry - 2 * rx*rx*y;
		}
	}

	while (y > 0)
	{
		Ellipses::setPixel(cx + x, cy + y);
		Ellipses::setPixel(cx - x, cy + y);
		Ellipses::setPixel(cx + x, cy - y);
		Ellipses::setPixel(cx - x, cy - y);

		y--;
		if (P < 0)
		{
			P += 2 * rx*rx *y + rx*rx;
		}
		else
		{
			x++;
			P += 2 * rx*rx *y + rx*rx - 2 * ry*ry*x;
		}
	}
}

void Ellipses::render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	Ellipses::MidPoint(500, 500, 200, 300);
	glFlush();
}

void Ellipses::reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspectRatio = (float)width / height;

	if (width > height)
		gluOrtho2D(0, 1000.0 * aspectRatio, 0, 1000);
	else
		gluOrtho2D(0, 1000.0, 0, 1000.0 / aspectRatio);

	glViewport(0, 0, width, height);
}

void Ellipses::main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutCreateWindow("Test OpenGL");

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0, 1000.0, 0, 1000);

	glutDisplayFunc(Ellipses::render);
	glutReshapeFunc(Ellipses::reshape);
	glutMainLoop();
}