#pragma once

namespace Ellipses
{
	void render();
	void reshape(int width, int height);
	void setPixel(int x, int y);
	void MidPoint(int cx, int cy, int rx, int ry);

	void main(int argc, char** argv);
};