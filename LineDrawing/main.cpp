#include <iostream>
#include <gl/glut.h>

using namespace std;

void init();

void bresenhamLineDraw(float x_start, float y_start, float x_end, float y_end);

void setPixel(float x, float y);

void render();

GLenum errorCheck();

void bhm_line(int x1, int y1, int x2, int y2);

float x_fir, y_fir, x_last, y_last;


int main(int argc, char **argv) {  // Initialize GLUT and
    printf("Please Enter X and Y coordinates for the First Point: ");
    cin >> x_fir >> y_fir;
    printf("Please Enter X and Y coordinates for the Second Point: ");
    cin >> x_last >> y_last;

    printf("%f %f %f %f", x_fir, y_fir, x_last, y_last);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);    // Use single color buffer and no depth buffer.
    glutInitWindowSize(400, 300);         // Size of display area, in pixels.
    glutInitWindowPosition(100, 100);     // Location of window in screen coordinates.
    glutCreateWindow("GL RGB Triangle"); // Parameter is window title.

    init();
    glutDisplayFunc(render);            // Called when the window needs to be redrawn.
    glutMainLoop(); // Run the event loop!  This function does not return.
    // Program ends when user closes the window.
    return 0;

}

void init() {
    glClearColor(1.0, 1.0, 1.0, 0.0); // Set display-window color to white.
    glMatrixMode(GL_PROJECTION); // Set projection parameters.
    glLoadIdentity();
    gluOrtho2D(0.0, 1000.0, 0.0, 1000.0);
}


void correctXOrder(float &x_start, float &y_start, float &x_end, float &y_end) {
    if (x_start > x_end) {
        float tempX = x_start;
        float tempY = y_start;
        x_start = x_end;
        y_start = y_end;
        x_end = tempX;
        y_end = tempY;
    }
}

void correctYOrder(float &x_start, float &y_start, float &x_end, float &y_end) {
    if (y_start > y_end) {
        float tempX = x_start;
        float tempY = y_start;
        x_start = x_end;
        y_start = y_end;
        x_end = tempX;
        y_end = tempY;
    }
}

void directScan(float x_start, float y_start, float x_end, float y_end) {
    float dy = y_end - y_start;
    float dx = x_end - x_start;
    float m = dy / dx;
    float b = y_start - m * x_start;
    char slope_sign = ((dy > 0 && dx > 0) || (dy < 0 && dx < 0)) ? '+' : '-';

    if (abs(m) > 1) {
        float x, y;
        correctYOrder(x_start, y_start, x_end, y_end);
        x = x_start;
        y = y_start;
        while (y <= y_end) {
            setPixel(x, y);
            x = y / m - b / m;
            ++y;
        }

    } else {
        float x, y;
        correctXOrder(x_start, y_start, x_end, y_end);
        x = x_start;
        y = y_start;
        while (x <= x_end) {
            setPixel(x, y);
            y = m * x + b;
            ++x;
        }
    }


}

void DDA(float x_start, float y_start, float x_end, float y_end) {
    float dx = x_end - x_start;
    float dy = y_end - y_start;

    float steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    float xi = dx / steps;
    float yi = dy / steps;

    auto x = x_start, y = y_start;

    for (int k = 0; k < steps; ++k) {
        setPixel(x, y);
        x = x + xi;
        y = y + yi;
    }
}

void DDA2(float x_start, float y_start, float x_end, float y_end) {
    float dx = x_end - x_start;
    float dy = y_end - y_start;
    float x, y, steps;
    if (abs(dy) > abs(dx)) {
        correctYOrder(x_start, y_start, x_end, y_end);
        x = x_start, y = y_start, steps = dx / dy;
        while (y <= y_end) {
            setPixel(x, y);
            ++y;
            x += steps;
        }

    } else {
        correctXOrder(x_start, y_start, x_end, y_end);
        x = x_start, y = y_start, steps = dy / dx;
        while (x <= x_end) {
            setPixel(x, y);
            ++x;
            y += steps;
        }
    }
}

void bresenhamLineDraw(float x_start, float y_start, float x_end, float y_end) {

    float dx = x_end - x_start;
    float dy = y_end - y_start;
    char slope_sign = ((dy > 0 && dx > 0) || (dy < 0 && dx < 0)) ? '+' : '-';
    dx = abs(dx);
    dy = abs(dy);
    float slope = dy / dx;
    float low_step, high_step;
    float x, y, x_final, y_final, dec_var;
    /* if the slope is greater than 1, then our equation will be Yk+1 = m(x)+c or x = 1/m(Yk+1 - c)  */

    if (slope > 1) { // if dy > dx
        low_step = 2 * dx;
        high_step = 2 * (dx - dy);
        dec_var = 2 * dx - dy;
    } else {
        low_step = 2 * dy;
        high_step = 2 * (dy - dx);
        dec_var = 2 * dy - dx;
    }


    if (slope <= 1) {
        // swap points if it is drawn from right to left
        if (x_start > x_end) {
            x = x_end;
            y = y_end;
            x_final = x_start;
        } else {
            x = x_start;
            y = y_start;
            x_final = x_end;
        }
        while (x <= x_final) {
            setPixel(x, y);
            if (dec_var < 0) { // p += 2 * dy
                dec_var += low_step;
            } else { // p += 2 * (dy - dx)
                dec_var += high_step;
                if (slope_sign == '+') {
                    y += 1;
                } else {
                    y -= 1;
                }
            }
            x += 1;
        }
    } else {
        if (y_start > y_end) {
            x = x_end;
            y = y_end;
            y_final = y_start;
        } else {
            x = x_start;
            y = y_start;
            y_final = y_end;
        }

        while (y <= y_final) {
            setPixel(x, y);
            if (dec_var < 0) {
                dec_var += low_step;
            } else {
                dec_var += high_step;
                if (slope_sign == '+') {
                    x += 1;
                } else {
                    x -= 1;
                }
            }
            y += 1;
        }
    }


}

void setPixel(float x, float y) {
    glBegin(GL_POINTS);
    glVertex2f(x, y);
    glEnd();
}

void render() {
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.5, 0.2, 0.7);
//    bresenhamLineDraw(x_fir, y_fir, x_last, y_last);
//    directScan(x_fir, y_fir, x_last, y_last);
    DDA2(x_fir, y_fir, x_last, y_last);
    errorCheck();
    glFlush();
}

GLenum errorCheck() {
    GLenum code;
    const GLubyte *string;
    code = glGetError();
    if (code != GL_NO_ERROR) {
        string = gluErrorString(code);
        fprintf(stderr, "OpenGL error: %s\n", string);
    }
    return code;
}