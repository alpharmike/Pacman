#include <GL/glut.h>
#include "Game.h"

Game game;

void render()
{
    glClear(GL_COLOR_BUFFER_BIT);

    int time = glutGet(GLUT_ELAPSED_TIME);
    game.update(time);
    game.render(time);

    glutSwapBuffers();
}

void init()
{
    glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
    int time = glutGet(GLUT_ELAPSED_TIME);
    glEnable(GL_LINE_STIPPLE);

    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



    game.load(time);
}

void keyboardHandler(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        case 'Q':
            exit(0);
            break;
        case 'r':
        case 'R':
            game.resetGame();
            game.load(0);
            break;
    }
}

void keyboard(int key, int x, int y)
{

    if (key == GLUT_KEY_F11) {
        game.toggleFullScreen();
        if (game.isFullScreen()) {
            glutFullScreen();
        } else {
            glutReshapeWindow(800, 600);
        }
    }
    int time = glutGet(GLUT_ELAPSED_TIME);

    game.keyboard(time, key, x, y);
}

void keyboard_up(int key, int x, int y)
{
    int time = glutGet(GLUT_ELAPSED_TIME);

    game.keyboard_up(time, key, x, y);
}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    float aspectRatio = (float)width / height;
    if (width > height)
    {
        gluOrtho2D(0, 1300, 0, 1000);
    }
    else
    {
        gluOrtho2D(0, 1300, 0, 1000);
    }

    glViewport(0, 0, width, height);
}

int main(int argc, char** argv)
{

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

    glutInitWindowSize(800, 600);
    glutCreateWindow("Pacman");

    glClearColor(0.0, 0.0, 0.0, 0.0);
    init();
    glutDisplayFunc(render);
    glutIdleFunc(render);
    glutReshapeFunc(reshape);
    glutSpecialFunc(keyboard);
    glutSpecialUpFunc(keyboard_up);
    glutKeyboardFunc(keyboardHandler);

    glutMainLoop();
}