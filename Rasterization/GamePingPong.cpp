#include <iostream>
#include <gl\glut.h>
#include "GamePingPong.h"

#define RACQUET 1000
#define BALL 1001
#define FIELD 1002

using namespace std;

PingPong::GameState game(1000, 500);

void PingPong::GameState::init()
{
	this->Player_1.Position = (this->FieldHeight - this->RaquetHeight) / 2;
	this->Player_2.Position = this->Player_1.Position;

	this->Ball.Position_X = this->FieldWidth / 2;
	this->Ball.Position_Y = this->FieldHeight / 2;

	auto theta = ((double)rand() / (RAND_MAX)) * 360;
	this->Ball.Velocity_X = cos(theta);
	this->Ball.Velocity_Y = sin(theta);
}

void PingPong::GameState::change_state(int player, int state)
{
	auto H = glutGet(GLUT_WINDOW_HEIGHT);

	if (player == 1)
		this->Player_1.Position =
		fmax(fmin(this->Player_1.Position + state * 50, this->FieldHeight - this->RaquetHeight), 0);
	else if (player == 2)
		this->Player_2.Position =
		fmax(fmin(this->FieldHeight - (float)state / H * this->FieldHeight, this->FieldHeight - this->RaquetHeight), 0);

	glutPostRedisplay();
}

bool Collision(int x0, int y0, int w0, int h0, int x1, int y1, int w1, int h1)
{
	auto l = fmax(x0, x1);
	auto t = fmin(y0, y1);

	auto r0 = x0 + w0, r1 = x1 + w1;
	auto b0 = y0 - h0, b1 = y1 - h1;

	auto b = fmax(b0, b1);
	auto r = fmin(r0, r1);

	return l <= r && t >= b;
}

void PingPong::GameState::next_state()
{
	Ball.Position_X += Ball.Velocity_X * 10;
	Ball.Position_Y += Ball.Velocity_Y * 10;
	
	if (Ball.Position_Y <= 0)
	{
		Ball.Position_Y = 0;
		Ball.Velocity_Y *= -1;
	}
	else if (Ball.Position_Y >= this->FieldHeight)
	{
		Ball.Position_Y = this->FieldHeight;
		Ball.Velocity_Y *= -1;
	}

	if (Collision(RaquetMargin, Player_1.Position, RaquetWidth, RaquetHeight,
		Ball.Position_X - BallSize / 2, Ball.Position_Y - BallSize / 2, BallSize, BallSize))
	{
		Ball.Position_X = RaquetMargin + RaquetWidth + BallSize;
		Ball.Velocity_X *= -1;
	}
	else if (Collision(FieldWidth - RaquetMargin - RaquetWidth, Player_2.Position, RaquetWidth, RaquetHeight,
		Ball.Position_X - BallSize / 2, Ball.Position_Y - BallSize / 2, BallSize, BallSize))
	{
		Ball.Position_X = FieldWidth - RaquetMargin - RaquetWidth - BallSize;
		Ball.Velocity_X *= -1;
	}

	if (Ball.Position_X < -BallSize * 2 || Ball.Position_X > FieldWidth + BallSize * 2)
	{
		init();
	}

	glutPostRedisplay();
	glutTimerFunc(20, PingPong::timer, 0);
}

void PingPong::keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'q':
	case 'Q':
		exit(0);
		break;
	case 'i':
	case 'I':
		game.init();
		break;
	}
}

void PingPong::keyboard_control(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		game.change_state(1, 1);
		break;
	case GLUT_KEY_DOWN:
		game.change_state(1, -1);
		break;
	default:
		game.change_state(1, 0);
		break;
	}	
}

void PingPong::keyboard_up(int key, int x, int y)
{
	game.change_state(1, 0);
}

void PingPong::mouse(int button, int state, int x, int y)
{
	cout << x << " , " << y << endl;
}

void PingPong::motion(int x, int y)
{
	game.change_state(2, y);
}

void PingPong::timer(int value)
{
	game.next_state();
}

void PingPong::render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glCallList(FIELD);

	glPushMatrix();
	glTranslatef(game.RaquetMargin, game.Player_1.Position, 0);
	glCallList(RACQUET);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(game.FieldWidth - game.RaquetMargin - game.RaquetWidth, game.Player_2.Position, 0);
	glCallList(RACQUET);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(game.Ball.Position_X, game.Ball.Position_Y, 0);
	glCallList(BALL);
	glPopMatrix();

	glutSwapBuffers();
}

void PingPong::init()
{
	game.init();

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 1000, 0, 1000);

	glNewList(FIELD, GL_COMPILE);
	Lines::Bresenham(0, 0, game.FieldWidth, 0);
	Lines::Bresenham(game.FieldWidth - 1, 0, game.FieldWidth - 1, game.FieldHeight);
	Lines::Bresenham(0, game.FieldHeight - 1, game.FieldWidth, game.FieldHeight - 1);
	Lines::Bresenham(0, 0, 0, game.FieldHeight);
	Lines::Bresenham(game.FieldWidth / 2, 0, game.FieldWidth / 2, game.FieldHeight);
	glEndList();

	glNewList(RACQUET, GL_COMPILE);
	Lines::Bresenham(0, 0, game.RaquetWidth, 0);
	Lines::Bresenham(game.RaquetWidth, 0, game.RaquetWidth, game.RaquetHeight);
	Lines::Bresenham(game.RaquetWidth, game.RaquetHeight, 0, game.RaquetHeight);
	Lines::Bresenham(0, 0, 0, game.RaquetHeight);
	glEndList();

	glNewList(BALL, GL_COMPILE);
	Circles::ParametricEquation(game.BallSize / 2, game.BallSize / 2, game.BallSize);
	glEndList();
}

void PingPong::reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	auto aspectRatio = (float)game.FieldWidth / game.FieldHeight;
	gluOrtho2D(0, game.FieldWidth, -game.FieldWidth / aspectRatio / 2 + game.FieldHeight / 2, game.FieldWidth / aspectRatio / 2 + game.FieldHeight / 2);

	glViewport(5, 5, width - 10, height - 10);
}

void PingPong::main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutCreateWindow("Ping Pong");
	//glutFullScreen();

	PingPong::init();

	glutDisplayFunc(PingPong::render);
	glutReshapeFunc(PingPong::reshape);
	glutKeyboardFunc(PingPong::keyboard);
	glutSpecialUpFunc(PingPong::keyboard_up);
	glutSpecialFunc(PingPong::keyboard_control);
	glutMouseFunc(PingPong::mouse);
	glutPassiveMotionFunc(PingPong::motion);
	glutTimerFunc(20, PingPong::timer, 0);

	glutMainLoop();
}