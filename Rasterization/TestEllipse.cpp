#include <Windows.h>
#include <iostream>
#include <gl/glut.h>
#include <chrono>

#include "Ellipses.h"

#define M_PI 3.14159265358979323846

using namespace std;

void Ellipses::setPixel(int x, int y)
{
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

void Ellipses::MidPoint(int cx, int cy, int rx, int ry)
{
	int x = 0;
	int y = ry;

	float p = ry*ry - rx*rx*ry + rx*rx / 4;

	while (ry*ry*x < rx*rx*y)
	{
		Ellipses::setPixel(cx + x, cy + y);
		Ellipses::setPixel(cx - x, cy + y);
		Ellipses::setPixel(cx + x, cy - y);
		Ellipses::setPixel(cx - x, cy - y);

		x++;
		if (p < 0)
		{
			p += 2 * ry*ry*x + ry*ry;
		}
		else
		{
			p += 2 * ry*ry*x + ry*ry - 2 * rx*rx*y;
			y--;
		}
	}

	while (y > 0)
	{
		Ellipses::setPixel(cx + x, cy + y);
		Ellipses::setPixel(cx - x, cy + y);
		Ellipses::setPixel(cx + x, cy - y);
		Ellipses::setPixel(cx - x, cy - y);

		y--;
		if (p < 0)
		{
			p += 2 * rx*rx*y + rx*rx;
		}
		else
		{
			p += 2 * rx*rx*y + rx*rx - 2 * ry*ry*x;
			x++;
		}
	}
}

void Ellipses::render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	Ellipses::MidPoint(500, 500, 200, 300);
	glFlush();
}

void Ellipses::reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspestRatio = (float)width / height;
	if (width > height)
	{
		gluOrtho2D(0, 1000 * aspestRatio, 0, 1000);
	}
	else
	{
		gluOrtho2D(0, 1000, 0, 1000 / aspestRatio);
	}

	glViewport(0, 0, width, height);
}

void Ellipses::main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutCreateWindow("Test OpenGL");

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0, 1000, 0, 1000);

	glutDisplayFunc(Ellipses::render);
	glutReshapeFunc(Ellipses::reshape);

	glutMainLoop();
}