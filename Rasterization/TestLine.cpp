#include <Windows.h>
#include <iostream>
#include <gl/glut.h>

using namespace std;

HWND consoleWindow2 = GetConsoleWindow();
HDC consoleDC2 = GetDC(consoleWindow2);

void drawSquare2()
{
	for (int i = 0; i < 100; i++)
		for (int j = 0; j < 100; j++)
			SetPixel(consoleDC2, i + 20, j + 20, RGB(i + j, i + 50, j + 100));
}

void setPixel2(int x, int y)
{
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

void DirectScan2(int x0, int y0, int x_end, int y_end)
{
	float m = (float)(y_end - y0) / (x_end - x0);
	float b = y0 - m*x0;

	auto x = x0;
	while (x <= x_end)
	{
		auto y = round(m*x + b);
		setPixel2(x++, y);
	}
}

void DDA2(int x0, int y0, int x_end, int y_end)
{
	int dx = x_end - x0;
	int dy = y_end - y0;

	float steps = dx > dy ? dx : dy;
	float xi = dx / steps, yi = dy / steps;

	float x = x0, y = y0;

	for (int k = 0; k < steps; k++)
	{
		setPixel2(x, round(y));

		x += xi;
		y += yi;
	}
}

void Bresenham2(int x0, int y0, int x_end, int y_end)
{
	int dx = x_end - x0;
	int dy = y_end - y0;

	int p = 2 * dy - dx;
	float x = x0, y = y0;

	while (x <= x_end)
	{
		setPixel2(x++, y);

		if (p < 0)
		{
			p += 2 * dy;
		}
		else
		{
			p += 2 * dy - 2 * dx;
			y += 1;
		}
	}
}

void Parallel2(int x0, int y0, int x_end, int y_end)
{
	int dx = x_end - x0;
	int dy = y_end - y0;

	float ll = sqrt(pow(dx, 2) + pow(dy, 2));
	float A = -dy / ll;
	float B = dx / ll;
	float C = (x0 * dy - y0 * dx) / ll;

	for (int x = x0; x < x_end; x++)
	{
		for (int y = y0; y < y_end; y++)
		{
			float d = A *x + B*y + C;
			if (abs(d) <= 2) setPixel2(x, y);
		}
	}
}

void Polygon2(int xs[], int ys[], int pointCount)
{
	for (int i = 0; i < pointCount - 1; i++)
		DDA2(xs[i], ys[i], xs[i + 1], ys[i + 1]);

	DDA2(xs[pointCount - 1], ys[pointCount - 1], xs[0], ys[0]);
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 0.0, 1.0);
	DirectScan2(300, 500, 700, 600);
	glColor3f(0.0, 0.0, 0.0);
	DDA2(300, 400, 700, 500);;
	glColor3f(0.0, 1.0, 0.0);
	Bresenham2(300, 300, 700, 400);
	glColor3f(0.0, 1.0, 1.0);
	Parallel2(300, 200, 700, 300);

	int xs[] = { 500, 600, 700, 600, 500, 400 };
	int ys[] = { 500, 200, 400, 700, 600, 500 };

	glColor3f(0.0, 0.0, 0.0);
	Polygon2(xs, ys, 6);

	glFlush();
}

void main_testline(int argc, char** argv)
{
	//drawSquare2();
	//ReleaseDC(consoleWindow2, consoleDC2);
	//cin.ignore();

	//return;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600);
	glutCreateWindow("Test OpenGL");

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0, 1000, 0, 1000);

	glutDisplayFunc(render);
	glutMainLoop();
}